#!/bin/sh

../google_appengine/appcfg.py upload_data --email=garcia.marc@gmail.com --url=http://localhost:8080/_ah/remote_api --application="dev~emasaje-pro" --kind=MassageMethod --filename=data/massagemethod.sqlite3 ../app
../google_appengine/appcfg.py upload_data --email=garcia.marc@gmail.com --url=http://localhost:8080/_ah/remote_api --application="dev~emasaje-pro" --kind=Account --filename=data/account.sqlite3 ../app
../google_appengine/appcfg.py upload_data --email=garcia.marc@gmail.com --url=http://localhost:8080/_ah/remote_api --application="dev~emasaje-pro" --kind=Page --filename=data/page.sqlite3 ../app
../google_appengine/appcfg.py upload_data --email=garcia.marc@gmail.com --url=http://localhost:8080/_ah/remote_api --application="dev~emasaje-pro" --kind=SearchPage --filename=data/searchpage.sqlite3 ../app
../google_appengine/appcfg.py upload_data --email=garcia.marc@gmail.com --url=http://localhost:8080/_ah/remote_api --application="dev~emasaje-pro" --kind=Contact --filename=data/contact.sqlite3 ../app

../google_appengine/appcfg.py upload_data --email=garcia.marc@gmail.com --url=http://localhost:8080/_ah/remote_api --application="dev~emasaje-pro" --kind=Region --filename=data/regions.csv --config_file=bulkloader.yaml --batch_size=100 --num_threads=4 ../app

