#!/bin/sh

../google_appengine/appcfg.py download_data -A s~emasaje-pro --url=http://emasaje-pro.appspot.com/_ah/remote_api/ --kind=MassageMethod --filename=data/massagemethod.sqlite3 ../app
../google_appengine/appcfg.py download_data -A s~emasaje-pro --url=http://emasaje-pro.appspot.com/_ah/remote_api/ --kind=Account --filename=data/account.sqlite3 ../app
../google_appengine/appcfg.py download_data -A s~emasaje-pro --url=http://emasaje-pro.appspot.com/_ah/remote_api/ --kind=Page --filename=data/page.sqlite3 ../app
../google_appengine/appcfg.py download_data -A s~emasaje-pro --url=http://emasaje-pro.appspot.com/_ah/remote_api/ --kind=SearchPage --filename=data/searchpage.sqlite3 ../app
../google_appengine/appcfg.py download_data -A s~emasaje-pro --url=http://emasaje-pro.appspot.com/_ah/remote_api/ --kind=Contact --filename=data/contact.sqlite3 ../app

