#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os
import glob
import re
import codecs
import unicodedata
import gzip
import logging

OUTPUT_FILE = 'some_regions.csv'
CMD = 'python ../../google_appengine/appcfg.py upload_data --config_file=bulkloader.yaml --filename=%s --kind=Region --url=http://localhost:8080/remote_api ../../app' % OUTPUT_FILE

(
    GEONAMEID,
    NAME,
    ASCIINAME,
    ALTERNATENAMES,
    LATITUDE,
    LONGITUDE,
    FEATURE_CLASS,
    FEATURE_CODE,
    COUNTRY_CODE,
    CC2,
    ADMIN1_CODE,
    ADMIN2_CODE,
    ADMIN3_CODE,
    ADMIN4_CODE,
    POPULATION,
    ELEVATION,
    GTOPO30,
    TIMEZONE,
    MODIFICATION_DATE,
) = range(0, 19)

KEYS = {
    'country': COUNTRY_CODE,
    'state': ADMIN1_CODE,
    'province': ADMIN2_CODE,
}
PARENT_KEYS = {
    'state': COUNTRY_CODE,
    'province': ADMIN1_CODE,
    'city': ADMIN2_CODE,
}

FEATURE_IDS = {
    'country': ('PCLI',),
    'state': ('ADM1',),
    'province': ('ADM2',),
    'city': ('PPL', 'PPLA', 'PPLA2', 'PPLA3', 'PPLA4', 'PPLC', 'PPLF', 'PPLG', 'PPLL', 'PPLR', 'PPLS'),
}

LOOKUPS = {
    'country': {
        u'Kingdom of Spain': u'España',
    },
    'state': {
        u'Euskal Autonomia Erkidegoa': u'Euskadi',
        u'Comunidad Autónoma de Cantabria': u'Cantabria',
        u'Comunidad Autónoma de Aragón': u'Aragón',
        u'Comunidad Autónoma de Canarias': u'Canarias',
        u'Comunitat Autònoma de les Illes Balears': u'Illes Balears',
        u'Región de Murcia': u'Murcia',
    },
}

def slugify(value):
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
    value = unicode(re.sub('[^\w\s-]', '', value).strip().lower())
    return re.sub('[-\s]+', '-', value)

def exec_cmd():
    os.system(CMD)
    for logfile in glob.glob('bulkloader-*'):
        os.remove(logfile)

def get_csv_data(d, kind, keys, parent_keys):
    '''
        d: row from geonames file
        kind: country | state | province | city
        keys: ids and slugs of the current kind
        parent_keys: ids and slug of parent kind
    '''
    geonamesid = d[GEONAMEID]
    name = LOOKUPS.get(kind, {}).get(d[NAME], d[NAME])
    slug = slugify(name)

    parent = dict(geonamesid=u'', slug=u'')
    parent_key_field = PARENT_KEYS.get(kind)
    if parent_key_field:
        parent = parent_keys.get(d[parent_key_field], parent)
    if parent['slug']:
        slug = parent['slug'] + '/' + slug

    if kind != 'city':
        keys[d[KEYS[kind]]] = dict(geonamesid=geonamesid, slug=slug)

    if kind == 'country' or parent['geonamesid']:
        res = []
        res.append(geonamesid)
        res.append(u'"' + name + u'"')
        res.append(parent['geonamesid'])
        res.append(slug)
        return u','.join(res) + u'\n'
    else:
        return None

def load_region_kind(filename, kind, parent_keys):
    f_in = gzip.open(filename, 'r')
    if kind == 'country':
        f_out = codecs.open(OUTPUT_FILE, 'w', encoding='utf-8')
        f_out.write('geonames_id,name,geonames_parent_id,slug\n')
    else:
        f_out = codecs.open(OUTPUT_FILE, 'a', encoding='utf-8')
    keys = {}
    for line in f_in.readlines():
        line = line.decode('utf-8')
        d = line.split('\t')
        if d[FEATURE_CODE] in FEATURE_IDS[kind]:
            csv_data = get_csv_data(d, kind, keys, parent_keys)
            if csv_data:
                f_out.write(csv_data)
            else:
                logging.warning('Incorrect data for record "%s" - "%s"' % (d[GEONAMEID], d[NAME]))

    f_out.close()
    f_in.close()

    return keys

def main(filename):
    keys = {}
    for kind in ('country', 'state', 'province', 'city'):
        keys = load_region_kind(filename, kind, keys)
    exec_cmd()

if __name__ == '__main__':
    if len(sys.argv) > 1 and os.path.isfile(sys.argv[1]):
        main(sys.argv[1])
    else:
        sys.stderr.write('Usage: %s <geonames_file>\n\n' % sys.argv[0])
        sys.stderr.write('\tgeonames_file: Gzipped geonames file\n')

