# -*- coding: utf-8 -*-
import datetime
import logging
import urllib
import json
from google.appengine.ext import webapp
from google.appengine.ext import db
from google.appengine.api import taskqueue
from google.appengine.api import urlfetch
from google.appengine.api import mail
from lib.sessions import Sessions
from lib.template import render
from apps.pages.models import Page
from apps.accounts.models import Account
import settings

FETCH = 10

class SessionCleanUp(webapp.RequestHandler):
    def get(self):
        one_week_ago = datetime.datetime.now() - datetime.timedelta(days=7)

        cnt = 0
        more_sessions = True
        while more_sessions:
            sessions = Sessions.all().filter('last_used < ', one_week_ago).fetch(FETCH)
            if len(sessions) < FETCH:
                more_sessions = False
            for session in sessions:
                session.delete()
                cnt += 1

        logging.info('SessionCleanUp: %s sessions deleted' % cnt)

class GeocodeOne(webapp.RequestHandler):
    def post(self):
        page_key = self.request.get('key')
        page = Page.get(page_key)
        if page and not page.position:
            address = u'%s, %s, %s, %s' % (page.address_1, page.city_name, page.province_name, page.country_name)
            url = u'http://maps.googleapis.com/maps/api/geocode/json?%s' % urllib.urlencode(dict(address=address.encode('utf-8'), sensor='false'))
            response = urlfetch.fetch(url).content
            response = json.loads(response)
            if response['status'] == 'OK':
                location = response['results'][0]['geometry']['location']
                page.position = db.GeoPt(location['lat'], location['lng'])
                logging.info('GeocodeOne: Successfully geocoded "%s" with address "%s"' % (page.slug, address))
            else:
                logging.warning('GeocodeOne: Unable to geocode "%s" with address "%s"' % (page.slug, address))
        else:
            logging.error('GeocodeOne: Page not found for key "%s"' % page_key)
        # we force save to reassign sectors in case settings have been changed
        page.put()

class GeocodeAll(webapp.RequestHandler):
    def get(self):
        page_list = Page.all().filter('is_completed = ', True)
        for page in page_list:
            taskqueue.add(url='/tasks/geocode-one/', params=dict(key=page.key()))

        logging.info('GeocodeAll: %s pages sent to geocode' % len(page_list))

class TouchAll(webapp.RequestHandler):
    def get(self):
        page_list = Page.all().filter('is_completed = ', True)
        for page in page_list:
            page.put()

        logging.info('TouchAll: %s pages updated' % len(page_list))

class NewsletterOne(webapp.RequestHandler):
    def post(self):
        account_key = self.request.get('key')
        account = Account.get(account_key)
        mail.send_mail(
            sender=settings.MAIL_FROM,
            to=account.email,
            subject=u'Consigue más visitas en tu página web',
            body=render(
                'newsletter/201103.plain',
                dict(name=account.name),
            )
        )
        logging.debug('Newsletter: "%s" emailed' % account.email)

class NewsletterAll(webapp.RequestHandler):
    def get(self):
        cnt = 0
        for account in Account.all():
            taskqueue.add(url='/tasks/newsletter-one/', params=dict(key=account.key()), queue_name='newsletter')
            cnt += 1
        logging.info('Newsletter: %s emails queued' % cnt)

