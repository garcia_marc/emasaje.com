# -*- coding: utf-8 -*-
from google.appengine.ext import webapp
from lib.template import render_response
import settings

class LegalHandler(webapp.RequestHandler):
    def get(self):
        render_response(self, self.template, {'site': self.request.application_url.split('//')[1], 'email': settings.MAIL_TO})

class PrivacyPolicy(LegalHandler):
    template = 'privacy_policy.html'

class TermsConditions(LegalHandler):
    template = 'terms_and_conditions.html'
