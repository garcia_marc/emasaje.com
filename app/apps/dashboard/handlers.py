# -*- coding: utf-8 -*-
import datetime
import logging
import settings
from google.appengine.ext import webapp
from google.appengine.ext import db
from lib.http import http404
from lib.template import render_response
from apps.accounts.utils import login_required
from apps.pages.models import Page
from apps.navigation.models import Region, MassageMethod
import datafuncs
import forms

class BaseHandler(webapp.RequestHandler):
    form_class = None
    template_name = None

    def _getobj(self, account):
        page_list = Page.all().filter('slug =', account['slug']).fetch(1)
        if len(page_list) > 0:
            return page_list[0]
        else:
            page = Page(
                slug=account['slug'],
                active=False)
            page.put()
            return page

    def _setcompleted(self, obj):
        required_fields = (
            ('page_title', (None, '')),
            ('page_content', (None, '')),
            ('page_theme', (None,)),
            ('page_image', (None,)),
            ('massage_method_ids', (None, [])),
            ('city_id', (None, '')),
            ('postal_code', (None, '')),
            ('address_1', (None, '')),
        )
        field_missing = False
        for field in required_fields:
            # comparing to None because 0 evaluates as False
            if getattr(obj, field[0]) in field[1]:
                field_missing = True
                break

        page_completed = not obj.active and not field_missing
        setattr(obj, 'active', not field_missing)

        return page_completed

    def getdata(self, session, form):
        raise NotImplementedError

    def setdata(self, session, form):
        raise NotImplementedError

    @login_required
    def get(self, account):
        form = self.form_class(self)
        obj = self._getobj(account)
        self.getdata(obj, form)
        render_response(self, self.template_name, {'form': form, 'account': account})

    @login_required
    def post(self, account):
        form = self.form_class(self)
        if form.is_valid():
            obj = self._getobj(account)
            self.setdata(obj, form)
            suffix = ''
            if self._setcompleted(obj):
                suffix = '?page-completed'
            obj.put()
            self.redirect('/dashboard/' + suffix)
        else:
            render_response(self, self.template_name, {'form': form, 'account': account})

class GalleryHandler(webapp.RequestHandler):
    @login_required
    def get(self, account):
        page = Page.get_by_slug(account['slug'])
        if page:
            render_response(self, self.template, {
                'items': self.items.values(),
                'selected_item': getattr(page, self.db_field),
                'account': account})
        else:
            logging.error('Page not found for account: %s' % account['key'])
            http404(self)

    @login_required
    def post(self, account):
        item = self.request.get('item')
        try:
            item = int(item)
        except ValueError:
            pass
        if item in self.items.keys():
            page = Page.get_by_slug(account['slug'])
            if page:
                setattr(page, self.db_field, item)
                page.put()
                self.redirect('/dashboard/')
            else:
                logging.error('Page not found for account: %s' % account['key'])
                http404(self)
        else:
            logging.error('Selected item from gallery does not exist: %s' % item)
            http404(self)

class Home(webapp.RequestHandler):
    @login_required
    def get(self, account):
        try:
            page = Page.all().filter('slug =', account['slug']).fetch(1)[0]
        except IndexError:
            logging.error('Page does not exist for account: %s' % account['key'])
        render_response(self, 'dashboard_home.html', {'page': page, 'account': account})

class Address(BaseHandler):
    form_class = forms.AddressForm
    template_name = 'dashboard_address.html'

    def _get_region_name(self, region_id):
        name = None
        if region_id:
            try:
                name = Region.all().filter('geonames_id = ', region_id).fetch(1)[0].name
            except IndexError:
                logging.error('Region not found: %s' % region_id)
        return name

    def getdata(self, obj, form):
        form['country'] = obj.country_id

        # TODO add state data when launched internationally
        form['state'] = obj.state_id

        if obj.state_id:
            form.province.data_func = datafuncs.get_regions(obj.state_id)
        form['province'] = obj.province_id

        if obj.province_id:
            form.city.data_func = datafuncs.get_regions(obj.province_id)
        form['city'] = obj.city_id

        form['postal_code'] = obj.postal_code
        form['address_1'] = obj.address_1
        form['address_2'] = obj.address_2
        form['show_address_in_page'] = obj.show_address_in_page

        if obj.position:
            form['latitude'] = obj.position.lat
            form['longitude'] = obj.position.lon

    def setdata(self, obj, form):
        obj.country_id = form.country.value
        obj.country_name = self._get_region_name(obj.country_id)

        obj.state_id = form.state.value
        obj.state_name = self._get_region_name(obj.state_id)

        obj.province_id = form.province.value
        obj.province_name = self._get_region_name(obj.province_id)

        obj.city_id = form.city.value
        obj.city_name = self._get_region_name(obj.city_id)

        obj.postal_code = form.postal_code.value
        obj.address_1 = form.address_1.value
        obj.address_2 = form.address_2.value
        obj.show_address_in_page = form.show_address_in_page.value

        if form.latitude.value and form.longitude.value:
            obj.position = db.GeoPt(form.latitude.value, form.longitude.value)
        else:
            obj.position = None

class ContactInfo(BaseHandler):
    form_class = forms.ContactInfoForm
    template_name = 'dashboard_contactinfo.html'

    def getdata(self, obj, form):
        form['phone_number_1'] = obj.phone_number_1
        form['phone_number_2'] = obj.phone_number_2
        form['website_address'] = obj.website_address

    def setdata(self, obj, form):
        obj.phone_number_1 = form.phone_number_1.value
        obj.phone_number_2 = form.phone_number_2.value
        obj.website_address = form.website_address.value

class PageContent(BaseHandler):
    form_class = forms.PageForm
    template_name = 'dashboard_page.html'

    def getdata(self, obj, form):
        form['page_title'] = obj.page_title
        form['page_content'] = obj.page_content

    def setdata(self, obj, form):
        obj.page_title = form.page_title.value
        obj.page_content = form.page_content.value

class MassageMethods(BaseHandler):
    form_class = forms.MassageMethodsForm
    template_name = 'dashboard_massage_methods.html'

    def getdata(self, obj, form):
        form['massage_methods'] = obj.massage_method_ids

    def setdata(self, obj, form):
        method_names = []
        method_ids = [int(val) for val in form.massage_methods.value]
        for method in MassageMethod.all():
            if method.old_id in method_ids:
                method_names.append(method.name)
        obj.massage_method_ids = method_ids
        obj.massage_method_names = method_names

class Schedule(BaseHandler):
    form_class = forms.ScheduleForm
    template_name = 'dashboard_schedule.html'

    def getdata(self, obj, form):
        form['schedule'] = obj.schedule

    def setdata(self, obj, form):
        obj.schedule = form.schedule.value

class ThemeGallery(GalleryHandler):
    template = 'dashboard_themes.html'
    db_field = 'page_theme'
    items = settings.PAGE_THEMES

class ImageGallery(GalleryHandler):
    template = 'dashboard_images.html'
    db_field = 'page_image'
    items = settings.PAGE_IMAGES

