from google.appengine.ext import webapp
from google.appengine.api import mail
from lib.template import render
from lib.i18n import gettext as _
from apps.accounts.models import Account
from apps.accounts.utils import login_required
from apps.navigation.models import Region
import settings

class BaseRegionService(webapp.RequestHandler):
    @login_required
    def get(self, account):
        try:
            parent_region_id = int(self.request.get(self.param))
        except ValueError:
            self.response.out.write(u'')
        else:
            regions = Region.all().filter('geonames_parent_id = ', parent_region_id).order('slug')
            html = u'<option value=""></option>'
            for region in regions:
                html += u'<option value="%s">%s</option>' % (region.geonames_id, region.name)
            self.response.out.write(html)

class States(BaseRegionService):
    param = 'country_id'

class Provinces(BaseRegionService):
    param = 'state_id'

class Cities(BaseRegionService):
    param = 'province_id'

class HelpRequest(webapp.RequestHandler):
    @login_required
    def post(self, account):
        if self.request.get('message'):
            referrer = self.request.headers.get('Referer')
            account_data = Account.get(account['key'])
            mail.send_mail(
                sender=settings.MAIL_FROM,
                to=settings.MAIL_TO,
                subject=_(u'Consulta en www.emasaje.com'),
                reply_to=account_data.email,
                body=render('mail/help_request.plain', {
                    'account': account_data,
                    'referrer': referrer,
                    'message': self.request.get('message'),
                })
            )
            self.response.out.write(_(u'El mensaje ha sido enviado correctamente'))

