# -*- coding: utf-8 -*-
from lib import forms, validators
from lib.i18n import gettext as _
import datafuncs

SPAIN_ID = 2510769 # TODO remove when launched internationally

class AddressForm(forms.Form):
    address_1 = forms.InputTextField(label=_(u'Dirección (Calle y número)'))
    address_2 = forms.InputTextField(label=_(u'Dirección (Piso, puerta, escalera)'))
    postal_code = forms.InputTextField(label=_(u'Código postal'))
    city = forms.SelectField(label=_(u'Ciudad'))
    province = forms.SelectField(label=_(u'Provincia'))
    state = forms.SelectField(label=_(u'Comunidad autónoma'), data_func=datafuncs.get_regions(SPAIN_ID)) # TODO data_func won't be set when launched internationally
    country = forms.SelectField(label=_(u'País'), data_func=datafuncs.get_countries) # TODO will be get_regions(None) when launched internationally
    show_address_in_page = forms.InputCheckboxField(label=_(u'Mostrar dirección en la página'))
    latitude = forms.InputHiddenField()
    longitude = forms.InputHiddenField()

class ContactInfoForm(forms.Form):
    phone_number_1 = forms.InputTextField(label=_(u'Número de teléfono'))
    phone_number_2 = forms.InputTextField(label=_(u'Número de teléfono adicional'))
    website_address = forms.InputTextField(label=_(u'Página web'), validator=validators.IsUrl)
    requires_appointment = forms.InputCheckboxField(label=_('Necesario solicitar cita previa'))

class PageForm(forms.Form):
    page_title = forms.InputTextField(label=_(u'Título de la página'))
    page_content = forms.TextareaField(label=_(u'Contenido de la página'))

class MassageMethodsForm(forms.Form):
    massage_methods = forms.MultiCheckboxField(label=_(u'Técnicas de masaje'), data_func=datafuncs.get_massage_methods)

class ScheduleForm(forms.Form):
    schedule = forms.TextareaField(label=_('Horario'))

class PasswordForm(forms.Form):
    old_password = forms.InputPasswordField(label=_(u'Contraseña actual'))
    new_password = forms.InputPasswordField(label=_(u'Contraseña nueva'))
    new_password_repeat = forms.InputPasswordField(label=_(u'Contraseña nueva (repetir)'))

    def validate(self):
        if self.new_password != self.new_password_repeat:
            return _(u'Los dos valores de la nueva contraseña no son iguales')
        elif len(self.new_password) < 4:
            return _(u'La nueva contraseña debe tener por lo menos 4 carácteres')
        else:
            return None

