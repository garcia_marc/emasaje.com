# -*- coding: utf-8 -*-
from apps.navigation.models import Region, MassageMethod

SPAIN_ID = 2510769 # TODO remove when launched internationally

def get_countries():
    # TODO when adding more countries, it has to return the list
    return ((SPAIN_ID, u'España'),)

def get_regions(parent_id):
    def get_filtered_regions():
        # TODO make more efficient
        data = []
        for region in Region.all().filter('geonames_parent_id = ', parent_id).order('slug'):
            data.append((region.geonames_id, region.name))
        return data
    return get_filtered_regions

def get_massage_methods():
    # TODO make more efficient
    data = []
    for massage_method in MassageMethod.all():
        data.append((massage_method.old_id, massage_method.name))
    return data

