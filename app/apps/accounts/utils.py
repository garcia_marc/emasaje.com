from lib.sessions import Session

def login_required(func):
    def protected_func(request_handler, *args, **kwargs):
        session = Session(request_handler)
        account = session.get('account')
        if account:
            func(request_handler, account, *args, **kwargs)
        else:
            request_handler.redirect('/accounts/login/')
    return protected_func

