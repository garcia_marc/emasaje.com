from google.appengine.ext import db

class Account(db.Model):
    email = db.EmailProperty(required=True, indexed=True)
    slug = db.StringProperty(indexed=False)
    name = db.StringProperty(indexed=False)
    active = db.BooleanProperty(indexed=False)
    blocked = db.BooleanProperty(indexed=False)
    password = db.StringProperty(indexed=False)
    creation_date = db.DateTimeProperty(required=True, indexed=False)
    random_key = db.StringProperty(indexed=False)
    activation_date = db.DateTimeProperty(indexed=False)
    password_recovery_until = db.DateTimeProperty(indexed=False)
    conditions_version = db.IntegerProperty(indexed=False)
    comment = db.TextProperty(indexed=False)

