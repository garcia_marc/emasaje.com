# -*- coding: utf-8 -*-
import datetime
import hashlib
import random
import logging
from google.appengine.ext import webapp
from google.appengine.api import mail
from lib.template import render_response, render
from lib.http import http404
from lib.i18n import gettext as _
from lib.sessions import Session
from apps.pages.models import Page
from apps.pages.forms import ContactForm
from apps.accounts.utils import login_required
import settings
import models
import forms

RANDOM_BITS = 128
PASSWORD_RECOVERY_DAYS = 1

def get_random_key(email):
    return hashlib.sha256(settings.SECRET_KEY + email + str(random.getrandbits(RANDOM_BITS))).hexdigest() 


class Info(webapp.RequestHandler):
    def get(self):
        render_response(self, 'info.html')


class SamplePage:
    page_title = u'Tu página en www.emasaje.com'
    page_content = (u'Aquí podrás contar quien eres, y convencer a los '
        u'visitantes de tu página para que vengan a tu consulta. El texto es '
        u'libre y no tienes ninguna limitación de carácteres.\n\n'
        u'A parte de esta sección, puedes ver que el resto de la página '
        u'contendrá todas las informaciones que los usuarios necesitan '
        u'para convencerse, contactarte, y venirse a hacer un masaje contigo.'
        u' Puedes poner la información de técnicas que trabajas, horario, y la'
        u'información de contacto que consideres más útil, dirección, teléfono, '
        u'otra página web,... Nosotros añadimos un formulario de contacto '
        u'a tu página, para que a los usuarios les sea más fácil contactarte,'
        u'y si alguien te escribe, te enviaremos un correo electrónico.\n\n'
        u'Y por si fuera poco, el color de fondo de la página, y la '
        u'imágen que aparece, también las puedes escojer.'
        u'En www.emasaje.com tú eliges los contenidos, y nosotros te creamos '
        u'tu página, así de fácil, así de simple, y totalmente gratis. :)\n\n'
        )
    show_address_in_page = True
    address_1 = 'Las Ramblas, 1'
    city_name = 'Barcelona'
    postal_code = '08002'
    phone_number_1 = '987 654 321'
    website_address = 'http://www.tu-otra-pagina.com'
    massage_method_names = ['Quiromasaje', u'Reflexología podal']
    schedule = u'De Lunes a Viernes, de 9h a 20h\nSábado de 9h a 14h'
    position = type('position', (), dict(lat=41.37669, lon=2.17663))


class Sample(webapp.RequestHandler):
    def get(self):
        render_response(self, 'page.html', {
            'page': SamplePage,
            'theme': settings.PAGE_THEMES[0][1],
            'image': settings.PAGE_IMAGES[0][1],
            'form': ContactForm(self),
        })

    def post(self):
        self.get()


class LoginAccount(webapp.RequestHandler):
    def get(self):
        session = Session(self)
        if session.get('account'):
            self.redirect('/dashboard/')
        else:
            form = forms.LoginForm(self)
            render_response(self, 'login.html', {'form': form})

    def post(self):
        login_ok = False
        form = forms.LoginForm(self)
        if form.is_valid():
            account_list = models.Account.all().filter('email =', form.email.value).fetch(1)
            if len(account_list) > 0:
                account = account_list[0]
                if hashlib.sha256(form.password.value).hexdigest() == account.password and account.blocked != True:
                    session = Session(self)
                    session['account'] = dict(key=account.key(), slug=account.slug, name=account.name)
                    session.commit()
                    login_ok = True

            if not login_ok:
                form.fields['password'].error = _(u'La dirección de correo electrónico y/o la contraseña no son válidas')

        if login_ok:
            if account.conditions_version != settings.CONDITIONS_VERSION:
                self.redirect('/accounts/pending-data')
            else:
                self.redirect('/dashboard/')
        else:
            render_response(self, 'login.html', {'form': form})

class LogoutAccount(webapp.RequestHandler):
    def post(self):
        session = Session(self)
        if session.get('account'):
            del session['account']
            session.commit()
        self.redirect('/')

class RegisterAccount(webapp.RequestHandler):
    def get(self):
        form = forms.RegisterForm(self)
        render_response(self, 'register.html', {'form': form})

    def post(self):
        form = forms.RegisterForm(self)
        if form.is_valid():
            activation_key = get_random_key(form.email.value)
            account = models.Account(
                email=form.email.value.lower(),
                slug=form.slug.value,
                name=form.name.value,
                active=False,
                blocked=False,
                password=hashlib.sha256(form.password.value).hexdigest(),
                creation_date=datetime.datetime.now(),
                random_key=activation_key,
                activation_date=None,
                conditions_version=settings.CONDITIONS_VERSION,
                comment=None)
            account.put()
            Page(
                account=account,
                email=form.email.value.lower(),
                slug=form.slug.value,
                active=False).put()

            ops_url = '%s/accounts/%s/%s/%s/' % (self.request.application_url, '%s', form.email.value.replace('@', '/'), activation_key)
            mail.send_mail(
                sender=settings.MAIL_FROM,
                to=form.email.value,
                subject=_(u'Creación de cuenta en www.emasaje.com'),
                body=render(
                    'mail/create_account.plain', {
                        'name': form.name.value,
                        'activation_url': ops_url % 'activate',
                        'block_url': ops_url % 'block'}))
            email_page = settings.EMAIL_PAGES.get(form.email.value.split('@',)[1], '')
            email_link = email_page and u' <a href="%s">Clica aquí</a> para ir a la página de tu correo electrónico.' % email_page
            render_response(self, 'message.html', {'ga': 'account-created', 'message': _(u'Debemos verificar tu cuenta de correo electrónico para poder activar tu cuenta. Te hemos enviado un mensaje a %s con un enlace al que debes clicar.%s' % (form.email.value, email_link))})
        else:
            logging.info('Registration form error (email=%s, slug=%s, name=%s)' % (form.email.value, form.slug.value, form.name.value))
            render_response(self, 'register.html', {'form': form})

class ActivateAccount(webapp.RequestHandler):
    def get(self, email_user, email_domain, activation_key):
        account_list = models.Account.all().filter('email =', '%s@%s' % (email_user, email_domain)).fetch(1)
        if len(account_list) > 0:
            account = account_list[0]
            if account.random_key == activation_key and account.active == False:
                render_response(self, 'account_verify.html', {})
            else:
                http404(self)
        else:
            http404(self)

    def post(self, email_user, email_domain, activation_key):
        account_list = models.Account.all().filter('email =', '%s@%s' % (email_user, email_domain)).fetch(1)
        if len(account_list) > 0:
            account = account_list[0]
            if account.random_key == activation_key and account.active == False:
                account.active = True
                account.activation_date = datetime.datetime.now()
                account.put()

                session = Session(self)
                session['account'] = dict(key=account.key(), slug=account.slug, name=account.name)
                session.commit()
                self.redirect('/dashboard/?account-verified')
            else:
                http404(self)
        else:
            http404(self)
    
class PasswordRecoveryStep1(webapp.RequestHandler):
    def get(self):
        form = forms.PasswordRecoveryEmailForm(self)
        render_response(self, 'password_recovery_1.html', {'form': form})

    def post(self):
        form = forms.PasswordRecoveryEmailForm(self)
        if form.is_valid():
            account_list = models.Account.all().filter('email =', form.email.value).fetch(1)
            if len(account_list) > 0 and account_list[0].active:
                recovery_key = get_random_key(form.email.value)
                account = account_list[0]
                account.random_key = recovery_key
                account.password_recovery_until = datetime.datetime.now() + datetime.timedelta(days=PASSWORD_RECOVERY_DAYS) 
                account.put()
                mail.send_mail(
                    sender=settings.MAIL_FROM,
                    to=form.email.value,
                    subject=_(u'Recuperación de contraseña en www.emasaje.com'),
                    body=render(
                        'mail/password_recovery.plain', {
                            'name': account.name,
                            'url': '%s/accounts/password-recovery/%s/%s/' % (
                                self.request.application_url, form.email.value.replace('@', '/'), recovery_key)}))
            render_response(self, 'message.html', {'message': _(u'Acabamos de enviar un mensaje a la dirección <b>%s</b> con instrucciones para establecer una nueva contraseña. Si la dirección introducida no se corresponde con la de ningún usuario de nuestro sistema, no se habrá enviado ningún mensaje, por lo que deberías verificar que la dirección sea correcta.' % form.email.value)})
        else:
            render_response(self, 'password_recovery_1.html', {'form': form})

class PasswordRecoveryStep2(webapp.RequestHandler):
    def get(self, email_user, email_domain, recovery_key):
        account_list = models.Account.all().filter('email =', '%s@%s' % (email_user, email_domain)).fetch(1)
        if len(account_list) > 0 and account_list[0].password_recovery_until > datetime.datetime.now():
            form = forms.PasswordRecoveryPasswordForm(self)
            render_response(self, 'password_recovery_2.html', {'form': form})
        else:
            http404(self)

    def post(self, email_user, email_domain, activation_key):
        account_list = models.Account.all().filter('email =', '%s@%s' % (email_user, email_domain)).fetch(1)
        if len(account_list) > 0 and account_list[0].password_recovery_until > datetime.datetime.now():
            form = forms.PasswordRecoveryPasswordForm(self)
            if form.is_valid():
                account = account_list[0]
                account.password=hashlib.sha256(form.password.value).hexdigest()
                account.put()
                session = Session(self)
                session['account'] = dict(key=account.key(), slug=account.slug, name=account.name)
                session.commit()
                self.redirect('/dashboard/')
            else:
                render_response(self, 'password_recovery_2.html', {'form': form})
        else:
            http404(self)

class PendingData(webapp.RequestHandler):
    @login_required
    def get(self, account):
        form = forms.PendingDataForm(self)
        render_response(self, 'pending_data.html', {'form': form})

    @login_required
    def post(self, account):
        account = models.Account.get(account['key'])
        form = forms.PendingDataForm(self)
        if form.is_valid():
            account.name = form.name.value
            account.conditions_version = settings.CONDITIONS_VERSION
            account.put()
            session = Session(self)
            session['account'] = dict(key=account.key(), slug=account.slug, name=account.name)
            session.commit()

            page_list = Page.all().filter('account = ', account).fetch(1)
            if page_list:
                page = page_list[0]
                page.slug = form.slug.value
                page.last_update = datetime.datetime.now()
                page.put()
            else:
                Page(
                    account=account,
                    slug=form.slug.value,
                    is_blocked=False,
                    is_completed=False,
                    is_promoted=False,
                    is_reviewed = False,
                    last_update=datetime.datetime.now(),
                ).put()

            logging.info('User "%s" completed pending data' % account.email)
            self.redirect('/dashboard/')
        else:
            logging.info('Pending data form error (email=%s, slug=%s, name=%s)' % (account.email, form.slug.value, form.name.value))
            render_response(self, 'pending_data.html', {'form': form})

