# -*- coding: utf-8 -*-
from lib import forms, validators
from lib.i18n import gettext as _
from apps.pages.models import Page
import models

class RegisterForm(forms.Form):
    name = forms.InputTextField(label=_(u'Nombre'), required=True)
    email = forms.InputTextField(label=_(u'Correo electrónico'), required=True, validator=validators.IsEmail)
    slug = forms.InputTextField(label=_(u'Nombre de usuario'), required=True, validator=validators.IsSlug)
    password = forms.InputPasswordField(label=_(u'Contraseña'), required=True, validator=validators.IsPassword)
    password_repeat = forms.InputPasswordField(label=_(u'Contraseña (repetir)'), required=True, validator=validators.IsPassword)
    conditions = forms.InputCheckboxField(label=_(u'He leído y acepto la <a href="/legal/privacy-policy" target="_blank">Política de privacidad</a> y las <a href="/legal/terms-and-conditions/" target="_blank">Condiciones de uso</a>'))

    def validate(self):
        result = {}
        if self.password.value != self.password_repeat.value:
            result['password_repeat'] = _(u'Los dos valores de la contraseña no son iguales')
        if self.email.value and len(models.Account.all().filter('email = ', self.email.value).fetch(1)) > 0:
            result['email'] = _(u'Esta dirección de correo ya está registrada')
        if self.slug.value and len(Page.all().filter('slug = ', self.slug.value).fetch(1)) > 0:
            result['slug'] = _(u'Esta dirección ya está siendo utilizada por otro usuario')
        if self.conditions.value != True:
            result['conditions'] = _(u'Es necesario aceptar las condiciones para poderse registrar en la página')
        return result

class LoginForm(forms.Form):
    email = forms.InputTextField(label=_(u'Correo electrónico'), required=True, validator=validators.IsEmail)
    password = forms.InputPasswordField(label=_(u'Contraseña'), required=True, validator=validators.IsPassword)

class PasswordRecoveryEmailForm(forms.Form):
    email = forms.InputTextField(label=_(u'Correo electrónico'), required=True, validator=validators.IsEmail)

class PasswordRecoveryPasswordForm(forms.Form):
    password = forms.InputPasswordField(label=_(u'Contraseña'), required=True, validator=validators.IsPassword)
    password_repeat = forms.InputPasswordField(label=_(u'Contraseña (repetir)'), required=True, validator=validators.IsPassword)

    def validate(self):
        result = {}
        if self.password.value != self.password_repeat.value:
            result['password_repeat'] = _(u'Los dos valores de la contraseña no son iguales')
        return result

class PendingDataForm(forms.Form):
    name = forms.InputTextField(label=_(u'Nombre'), required=True)
    slug = forms.InputTextField(label=_(u'Nombre de usuario'), required=True, validator=validators.IsSlug)
    conditions = forms.InputCheckboxField(label=_(u'He leído y acepto la <a href="/legal/privacy-policy" target="_blank">Política de privacidad</a> y las <a href="/legal/terms-and-conditions/" target="_blank">Condiciones de uso</a>'))

    def validate(self):
        result = {}
        if self.slug.value and len(Page.all().filter('slug = ', self.slug.value).fetch(1)) > 0:
            result['slug'] = _(u'Esta dirección ya está siendo utilizada por otro usuario')
        if self.conditions.value != True:
            result['conditions'] = _(u'Es necesario aceptar las condiciones para poderse registrar en la página')
        return result

