import random
from google.appengine.ext import db

class SearchPage(db.Model):
    slug = db.StringProperty(indexed=True)

    page_title = db.StringProperty(indexed=False)
    full_address = db.StringProperty(indexed=False)
    position = db.GeoPtProperty(indexed=False)

    city_id = db.IntegerProperty(indexed=True)
    lat_rural_sectors = db.ListProperty(int, indexed=True)
    lng_rural_sectors = db.ListProperty(int, indexed=True)
    lat_urban_sectors = db.ListProperty(int, indexed=True)
    lng_urban_sectors = db.ListProperty(int, indexed=True)
    massage_method_ids = db.ListProperty(int, indexed=True)

    rank = db.IntegerProperty(indexed=True)

    def put(self):
        self.rank = random.randint(0, 32000)
        super(SearchPage, self).put()


class RankData(db.Model):
    slug = db.StringProperty(indexed=False)

