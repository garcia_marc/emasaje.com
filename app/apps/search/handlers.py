# -*- coding: utf-8 -*-
from google.appengine.ext import webapp
from lib.template import render_response
from lib.utils import paginate
from apps.search.models import SearchPage
import settings

COORDINATES = ('lat', 'lng')
URBAN_AREA = 'urban'
RURAL_AREA = 'rural'


class MapData:
    def __init__(self, location=None):
        if location:
            self.lat, self.lng = map(float, location.split(','))
        else:
            self.lat = self.lng = None
        self.zoom = settings.MAP_URBAN_ZOOM

    def set_rural_zoom(self):
        self.zoom = settings.MAP_RURAL_ZOOM

    def __dict__(self):
        return dict(
            lat=self.lat,
            lng=self.lng,
            zoom=self.zoom)


class Search(webapp.RequestHandler):
    def _list(self):
        page_list = SearchPage.all()
        massage_method = int(self.request.get('massage_method') or 0)
        if massage_method != 0:
            page_list.filter('massage_method_ids = ', massage_method)
        return page_list

    def _list_by_city(self, city_id):
        page_list = self._list()
        page_list.filter('city_id = ', city_id)
        return page_list

    def _list_by_location(self, area_type):
        page_list = self._list()
        for coord in COORDINATES:
            page_list = page_list.filter('%s_%s_sectors = ' % (coord, area_type),
                        int(self.request.get('%s-%s-sector' % (coord, area_type))))
        return page_list

    def _paginate(self, page_list):
        next_result = self.request.get('next_result')
        return paginate(page_list, '-rank', settings.MAP_SHOW_RESULTS, next_result, int)

    def _get_filter_context(self):
        fields = ['massage_method', 'location']
        for coord in COORDINATES:
            for area_type in [URBAN_AREA, RURAL_AREA]:
                fields.append('%s-%s-sector' % (coord, area_type))

        res = {}
        for field in fields:
            res[field.replace('-', '_')] = self.request.get(field)
        return res

    def get(self):
        next_result = None
        city_id = int(self.request.get('city') or 0)
        location = self.request.get('location')
        map_data = MapData(location)

        if city_id != 0:
            page_list = self._list_by_city(city_id)
            page_list, next_result = self._paginate(page_list)
        elif location:
            page_list = self._list_by_location(URBAN_AREA)
            page_list, next_result = self._paginate(page_list)
            if len(page_list) < settings.MAP_URBAN_MIN_RESULTS:
                page_list = self._list_by_location(RURAL_AREA)
                page_list, next_result = self._paginate(page_list)
                map_data.set_rural_zoom()
        else:
            page_list = self._list()
            page_list, next_result = self._paginate(page_list)


        context = {
            'map':  vars(map_data),
            'page_list': page_list,
            'next_result': next_result}
        context.update(self._get_filter_context())

        if self.request.get('ajax'):
            import json
            self.response.out.write(json.dumps(context))
        else:
            render_response(self, 'search.html', context)

