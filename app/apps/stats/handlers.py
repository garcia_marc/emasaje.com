# -*- coding: utf-8 -*-
import datetime
from google.appengine.ext import webapp
from lib.ext.gdata.analytics import client
from lib.template import render_response
from lib.sessions import Session
from lib.i18n import gettext as _
from apps.accounts.utils import login_required
import settings

class BaseStats(webapp.RequestHandler):
    @login_required
    def get(self, account):
        today = datetime.date.today()
        if self.query_dict:
            self.query_dict.update( {
                'ids': 'ga:%s' % settings.ANALYTICS_PROFILE,
                'start-date': (today - datetime.timedelta(days=settings.STATS_PERIOD_DAYS)).strftime('%Y-%m-%d'),
                'end-date': today.strftime('%Y-%m-%d'),
                'filters': 'ga:pagePath==/%s/' % account['slug'],
            })
        else:
            raise NotImplementedError
        data_query = client.DataFeedQuery(self.query_dict)
        analytics_client = client.AnalyticsClient()
        login_res = analytics_client.client_login(
            settings.ANALYTICS_USER,
            settings.ANALYTICS_PASSWORD,
            settings.ANALYTICS_SOURCE,
            settings.ANALYTICS_SERVICE,
            settings.ANALYTICS_ACCOUNT_TYPE,
        )
        data_feed = analytics_client.get_data_feed(data_query, analytics_client.auth_token)
        stats = data_feed.entry
        render_response(self, 'stats.html', {
            'account': account,
            'authenticated': True,
            'period_days': settings.STATS_PERIOD_DAYS,
            'stats': stats,
            'dimensions': self.dimensions,
            'metrics': self.metrics,
            'report': self.report,
            'report_name': self.report_name,
        })

class StatsBySource(BaseStats):
    report = 'by-source'
    report_name = _(u'Estadísticas por página de origen')
    dimensions = (_(u'Página de origen'),)
    metrics = (_(u'Visitas'),)
    query_dict = {
        'dimensions': 'ga:source',
        'metrics': 'ga:visits',
        'sort': '-ga:visits',
        'max-results': '30',
    }

class StatsByRegion(BaseStats):
    report = 'by-region'
    report_name = _(u'Estadísticas por región')
    dimensions = (_(u'País'), _(u'Ciudad'))
    metrics = (_(u'Visitas'),)
    query_dict = {
        'dimensions': 'ga:country,ga:city',
        'metrics': 'ga:visits',
        'sort': '-ga:visits',
        'max-results': '30',
    }

class StatsByDate(BaseStats):
    report = 'by-date'
    report_name = _(u'Estadísticas por fecha')
    dimensions = (_(u'Fecha'),)
    metrics = (_(u'Visitas'),)
    query_dict = {
        'dimensions': 'ga:date',
        'metrics': 'ga:visits',
        'sort': '-ga:date',
        'max-results': '30',
    }

