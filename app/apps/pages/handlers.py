# -*- coding: utf-8 -*-
import datetime
from google.appengine.ext import webapp
from google.appengine.api import mail
from lib.template import render_response, render
from lib.http import http404
from lib.i18n import gettext as _ 
from apps.pages.models import Page
import settings
import models
import forms

class PageHandler(webapp.RequestHandler):
    def _get_context(self, page, form):
        return {
            'page': page,
            'theme': settings.PAGE_THEMES[page.page_theme][1],
            'image': settings.PAGE_IMAGES[page.page_image][1],
            'recaptcha_key': settings.RECAPTCHA_PUBLIC_KEY,
            'form': form}

    def get(self, slug):
        page = Page.get_by_slug(slug)
        if page and page.active:
            render_response(self, 'page.html',
                            self._get_context(page, forms.ContactForm(self)))
        else:
            http404(self)

    def post(self, slug):
        page = Page.get_by_slug(slug)
        if page and page.active:
            form = forms.ContactForm(self)
            if form.is_valid():
                models.Contact(
                    slug = slug,
                    contact_date = datetime.datetime.now(),
                    ip = self.request.remote_addr,
                    name = form.name.value,
                    email = form.email.value,
                    phone_number = form.phone_number.value,
                    message = form.message.value,
                ).put()

                mail.send_mail(
                    sender = settings.MAIL_FROM,
                    to = page.email,
                    reply_to = '%s <%s>' % (form.name.value, form.email.value),
                    subject = _(u'Has recibido un mensaje en www.emasaje.com'),
                    body = render(
                        'mail/page_contact.plain', {
                            'name': form.name.value,
                            'email': form.email.value,
                            'phone_number': form.phone_number.value,
                            'message': form.message.value}))
                form.clear_values()

            render_response(self, 'page.html', self._get_context(page, form))
        else:
            http404(self)

