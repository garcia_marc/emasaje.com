# -*- coding: utf-8 -*-
from lib import forms, validators
from lib.i18n import gettext as _

class ContactForm(forms.RecaptchaForm):
    name = forms.InputTextField(label=_(u'Nombre'), required=True)
    email = forms.InputTextField(label=_(u'Correo electrónico'), required=True, validator=validators.IsEmail)
    phone_number = forms.InputTextField(label=_(u'Número de teléfono'), validator=validators.IsPhone)
    message = forms.TextareaField(label=_(u'Mensaje'), required=True)

