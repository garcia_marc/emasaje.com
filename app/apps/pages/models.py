from google.appengine.ext import db
from apps.search.models import SearchPage
from lib.maps import get_lat_sectors, get_lng_sectors
import settings


class Page(db.Model):
    slug = db.StringProperty(required=True, indexed=True)
    email = db.EmailProperty(required=True, indexed=False)

    # page
    page_title = db.StringProperty(indexed=False)
    page_content = db.TextProperty(indexed=False)
    page_theme = db.IntegerProperty(indexed=False)
    page_image = db.IntegerProperty(indexed=False)

    # address
    country_id = db.IntegerProperty(indexed=False)
    country_name = db.StringProperty(indexed=False)
    state_id = db.IntegerProperty(indexed=False)
    state_name = db.StringProperty(indexed=False)
    province_id = db.IntegerProperty(indexed=False)
    province_name = db.StringProperty(indexed=False)
    city_id = db.IntegerProperty(indexed=False)
    city_name = db.StringProperty(indexed=False)
    postal_code = db.StringProperty(indexed=False)
    address_1 = db.StringProperty(indexed=False) # street and number
    address_2 = db.StringProperty(indexed=False) # floor, door, apt...
    show_address_in_page = db.BooleanProperty(indexed=False)
    position = db.GeoPtProperty(indexed=False)

    # contact info
    phone_number_1 = db.StringProperty(indexed=False)
    phone_number_2 = db.StringProperty(indexed=False)
    website_address = db.StringProperty(indexed=False)
    appointment_required = db.BooleanProperty(indexed=False)

    # massage info
    massage_method_ids = db.ListProperty(int, indexed=False)
    massage_method_names = db.ListProperty(unicode, indexed=False) # XXX for some reason app engine makes it required

    # misc
    schedule = db.TextProperty(indexed=False)

    active = db.BooleanProperty(indexed=False)

    @property
    def full_address(self):
        return '%s%s, %s' % (
            self.address_1 + ', ' if self.show_address_in_page else '',
            self.postal_code,
            self.city_name)

    @classmethod
    def get_by_slug(cls, slug):
        page_list = cls.all().filter('slug =', slug).fetch(1)
        if len(page_list) > 0:
            return page_list[0]
        else:
            return None

    def set_sectors(self, search_page):
        if self.position:
            search_page.lat_rural_sectors = get_lat_sectors(
                self.position.lat,
                settings.MAP_RURAL_SECTOR_LAT_SIZE,
                settings.MAP_RURAL_SECTOR_LAT_DIFF)
            search_page.lng_rural_sectors = get_lng_sectors(
                self.position.lon,
                settings.MAP_RURAL_SECTOR_LNG_SIZE,
                settings.MAP_RURAL_SECTOR_LNG_DIFF)
            search_page.lat_urban_sectors = get_lat_sectors(
                self.position.lat,
                settings.MAP_URBAN_SECTOR_LAT_SIZE,
                settings.MAP_URBAN_SECTOR_LAT_DIFF)
            search_page.lng_urban_sectors = get_lng_sectors(
                self.position.lon,
                settings.MAP_URBAN_SECTOR_LNG_SIZE,
                settings.MAP_URBAN_SECTOR_LNG_DIFF)

    def put(self):
        search_page = SearchPage.all().filter('slug =', self.slug).fetch(1)
        search_page = search_page[0] if len(search_page) > 0 else None
        if self.active:
            if not search_page:
                search_page = SearchPage(slug=self.slug)
            search_page.page_title = self.page_title
            search_page.full_address = self.full_address
            search_page.city_id = self.city_id
            search_page.position = self.position
            search_page.massage_method_ids = self.massage_method_ids
            self.set_sectors(search_page)
            search_page.put()
            # TODO create rank data
        elif search_page:
            search_page.delete()

        super(Page, self).put()


class Contact(db.Model):
    slug = db.StringProperty(required=True, indexed=True)
    contact_date = db.DateTimeProperty(indexed=True)
    ip = db.StringProperty(indexed=False)
    name = db.StringProperty(indexed=False)
    email = db.StringProperty(indexed=False)
    phone_number = db.StringProperty(indexed=False)
    message = db.TextProperty(indexed=False)

