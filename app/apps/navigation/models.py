from google.appengine.ext import db

class MassageMethod(db.Model):
    parent_old_id = db.IntegerProperty(indexed=True)
    slug = db.StringProperty(indexed=True)
    name = db.StringProperty(indexed=False)
    old_id = db.IntegerProperty(indexed=False)

class Region(db.Model):
    geonames_id = db.IntegerProperty(indexed=True)
    name = db.StringProperty(indexed=False)
    geonames_parent_id = db.IntegerProperty(indexed=True)
    slug = db.StringProperty(indexed=True)

