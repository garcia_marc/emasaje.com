import settings
from google.appengine.ext import webapp
from lib.template import render_response
from lib.http import http404
from lib.utils import paginate
from apps.search.models import SearchPage
import models


class Methods(webapp.RequestHandler):
    def get(self, slug=None):
        next_result = self.request.get('next_result')

        if slug:
            method = models.MassageMethod.all().filter('slug = ', slug).get()
            if method:
                qs = SearchPage.all().filter('massage_method_ids = ', method.old_id)
                results, next_result = paginate(qs, '-rank', settings.NAV_PAGINATE_BY, next_result, int)
                render_response(self, 'navigation_methods.html', {'name': method.name, 'pages': results, 'next_result': next_result})
            else:
                http404(self)
        else:
            qs = models.MassageMethod.all().order('slug')
            results, next_result = paginate(qs, 'slug', settings.NAV_PAGINATE_BY, next_result)
            render_response(self, 'navigation_method_list.html', {'methods': results, 'next_result': next_result})


class Region(webapp.RequestHandler):
    @staticmethod
    def _is_city(region_slug):
        return region_slug and len(region_slug.split('/')) >= 4

    def get(self, region_slug=None):
        region = regions = None

        next_result = self.request.get('next_result')

        if region_slug:
            regions = models.Region.all().filter('slug = ', region_slug).fetch(1)

        if region_slug and not regions:
            http404(self)
        else:
            if regions:
                region = regions[0]

            if self._is_city(region_slug):
                qs = SearchPage.all().filter('city_id = ', region.geonames_id)
                results, next_result = paginate(qs, '-rank', settings.NAV_PAGINATE_BY, next_result, int)
                render_response(self, 'navigation_cities.html', {'name': region.name, 'pages': results, 'next_result': next_result})
            else:
                qs = models.Region.all().filter('geonames_parent_id = ', region and region.geonames_id).order('slug')
                results, next_result = paginate(qs, 'slug', settings.NAV_PAGINATE_BY, next_result)
                render_response(self, 'navigation_regions.html', {'name': region and region.name, 'regions': results, 'next_result': next_result})

