# -*- coding: utf-8 -*-
import webapp2
from google.appengine.api import memcache
from lib.template import render
from apps.navigation.models import MassageMethod
import settings

class Home(webapp2.RequestHandler):
    def get(self):
        content = memcache.get('home')
        if not content or settings.DEBUG or True:
            content = render('home.html', {
                'methods': MassageMethod.all().order('slug'),
                'settings': settings,
            })
            memcache.add('home', content)
        self.response.out.write(content)

