import time
from google.appengine.ext import webapp
from lib.http import http404
from lib.template import render_response
from lib.sessions import Session
from apps.accounts.models import Account
from apps.pages.models import Page

class Impersonate(webapp.RequestHandler):
    def get(self):
        render_response(self, 'impersonate.html', {})

    def post(self):
        if self.request.get('kind') == 'email':
            email = self.request.get('email')
        elif self.request.get('kind') == 'slug':
            page_list = Page.all().filter('slug = ', self.request.get('slug')).fetch(1)
            if page_list:
                email = page_list[0].email
            else:
                http404(self)
                return

        account_list = Account.all().filter('email = ', email).fetch(1)
        if account_list:
            account = account_list[0]
            session = Session(self)
            session['account'] = dict(key=account.key(), slug=account.slug, name=account.name)
            session.commit()
            time.sleep(1)
            self.redirect('/dashboard/')
        else:
            http404(self)

class Newsletter(webapp.RequestHandler):
    def get(self):
        render_response(self, 'newsletter.html', {})
        
class Review(webapp.RequestHandler):
    def get(self):
        pages = Page.all().filter('is_completed = ', True).filter('is_reviewed = ', False).order('-last_update').fetch(1)
        render_response(self, 'review.html', {'pages': pages})
        
    def post(self):
        review = self.request.get('review')
        page = Page.get(self.request.get('page'))

        if review == 'impersonate':
            impersonate(self, page.account)
        else:
            if review == 'spam':
                page.account.delete()
                page.delete()
                return
            elif review == 'block':
                page.is_blocked = True
            else:
                try:
                    rank = int(review)
                except ValueError:
                    self.response.write('Invalid option: %s' % review)
                    return
                else:
                    page.content_rank = rank
                    if self.request.get('sex') == 'on':
                        page.offers_sex = True
                    else:
                        page.offers_sex = False

            page.is_reviewed = True
            page.put()

            self.get()

