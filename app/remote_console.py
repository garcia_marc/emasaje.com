#!/usr/bin/python
import os
import code
import getpass
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '../google_appengine'))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '../google_appengine/lib/yank/lib'))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '../google_appengine/lib/fancy_urllib'))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '../google_appengine/lib/webob'))

import google
from google.appengine.ext.remote_api import remote_api_stub
from google.appengine.ext import db

APP = 'emasaje-com'
HOST = '%s.appspot.com' % APP
REMOTE_API = '/remote_api'

def auth_func():
    return raw_input('Username:'), getpass.getpass('Password:')

remote_api_stub.ConfigureRemoteDatastore(APP, REMOTE_API, auth_func, HOST)
code.interact('App Engine interactive console for %s' % (APP,), None, locals())

