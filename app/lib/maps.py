EQUATOR_LENGTH = 40008.629
MERIDIAN_LENGTH = 20003.93

def lat_to_kms(value):
    return int((value + 90) / 180 * MERIDIAN_LENGTH)
    
def lng_to_kms(value):
    return int((value + 180) / 360 * EQUATOR_LENGTH)

def get_sectors(val, sector_size, sector_diff):
    aprox_start = val - sector_size
    start = int(aprox_start / sector_diff) * sector_diff

    result = []
    sector = start
    while sector <= val + sector_size:
        result.append(sector)
        sector += sector_diff

    return result

def get_lat_sectors(lat, sector_size, sector_diff):
    return get_sectors(lat_to_kms(lat), sector_size, sector_diff)

def get_lng_sectors(lng, sector_size, sector_diff):
    return get_sectors(lng_to_kms(lng), sector_size, sector_diff)

