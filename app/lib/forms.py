import urllib
import urllib2
import json
import cgi
from lib.i18n import gettext as _
import settings

class Field:
    extra_optional_args = []
    extra_mandatory_args = []
    is_multiple = False

    def __init__(self, label=None, name=None, required=False, validator=None, **kwargs):
        self.label = label
        self.field_name = name
        self.validator = validator
        self.required = required
        self.value = None
        self.error = None

        for arg in self.extra_optional_args:
            if arg in kwargs:
                setattr(self, arg, kwargs[arg])

        for arg in self.extra_mandatory_args:
            if arg in kwargs:
                setattr(self, arg, kwargs[arg])
            else:
                raise SyntaxError('%s requires argument %s' % (self.__class__, arg))

    def parse_value(self, value):
        return value

    @property
    def name(self):
        return self.field_name

    @property
    def html_id(self):
        return u'id_%s' % self.name

    @property
    def label(self):
        return self.label

    @property
    def html_label(self):
        if self.label:
            return u'<label for="%s">%s</label>' % (self.html_id, self.label)
        else:
            return u''

    @property
    def input(self):
        raise NotImplementedError

    @property
    def html_error(self):
        if self.error:
            return u'<p class="error-msg">%s</p>' % self.error
        else:
            return u''

    @property
    def html(self):
        return u'%s%s%s' % (self.html_label, self.input, self.html_error)

    def __unicode__(self):
        return self.html

    def __str__(self):
        return unicode(self)

class InputField(Field):
    extra_optional_args = ('max_length',)

    def get_html_value(self):
        if self.value:
            return u' value="%s"' % self.value
        else:
            return u''

    def get_html_class(self):
        return u''

    @property
    def input_type(self):
        return NotImplementedError

    @property
    def input(self):
        return u'<input type="%s" name="%s" id="%s"%s%s/>' % (
            self.input_type,
            self.name,
            self.html_id,
            self.get_html_class(),
            self.get_html_value(),
        )

class InputTextField(InputField):
    @property
    def input_type(self):
        return 'text'

    def get_html_class(self):
        return u' class="input_text"'

    def parse_value(self, value):
        return cgi.escape(value)

class InputPasswordField(InputField):
    @property
    def input_type(self):
        return 'password'

    def get_html_value(self):
        return u''

    def get_html_class(self):
        return u' class="input_password"'

    def parse_value(self, value):
        return cgi.escape(value)

class InputCheckboxField(InputField):
    @property
    def input_type(self):
        return 'checkbox'

    def get_html_class(self):
        return u' class="input_checkbox"'

    def get_html_value(self):
        if self.value:
            return u' checked="checked"'
        else:
            return u''

    def parse_value(self, value):
        return value == 'on'

class InputHiddenField(InputField):
    @property
    def input_type(self):
        return 'hidden'

    def parse_value(self, value):
        return cgi.escape(value)

class TextareaField(InputField):
    @property
    def input(self):
        return u'<textarea name="%s" id="%s">%s</textarea>' % (self.name, self.html_id, self.value or '')

    def parse_value(self, value):
        return cgi.escape(value)

class SelectField(Field):
    extra_optional_args = ('data_func',)

    @property
    def input(self):
        html = u'<select name="%s" id="%s">' % (self.name, self.html_id)
        if not self.required:
            html += u'<option value=""></option>'
        if hasattr(self, 'data_func') and callable(self.data_func):
            for row in self.data_func():
                if row[0] == self.value:
                    selected = ' selected="selected"'
                else:
                    selected = u''
                html += u'<option value="%s"%s>%s</option>' % (row[0], selected, row[1])
        html += u'</select>'
        return html

    def parse_value(self, value):
        return value and int(value) or None

class MultiCheckboxField(Field):
    is_multiple = True
    extra_optional_args = ('data_func',)

    @property
    def input(self):
        html = u''
        if hasattr(self, 'data_func') and callable(self.data_func):
            for row in self.data_func():
                item_id = u'%s_%s' % (self.html_id, row[0])
                if row[0] in self.value:
                    html_value = u' checked="checked"'
                else:
                    html_value = u''
                html += u'<div class="multi-checkbox">'
                html += u'<label for="%s">%s</label>\n' % (item_id, row[1])
                html += u'<input type="checkbox" name="%s" value="%s" id="%s" class="input_checkbox"%s/>\n' % (self.name, row[0], item_id, html_value)
                html += u'</div>'
        return html

    @property
    def html(self):
        return u'%s%s' % (self.input, self.html_error)

class FormMetaClass(type):
    def __new__(cls, name, bases, attrs):
        fields = {}
        for key, val in attrs.iteritems():
            if isinstance(val, Field):
                if not val.name:
                    val.name = key
                fields[key] = val
        attrs['fields'] = fields
        return super(FormMetaClass, cls).__new__(cls, name, bases, attrs)

class Form:
    __metaclass__ = FormMetaClass

    def __init__(self, request_handler):
        self.valid = None
        self.request_handler = request_handler
        for field_name, field_obj in self.fields.iteritems():
            field_obj.value = None
            field_obj.error = None
            if request_handler.request.method == 'POST':
                if field_obj.is_multiple:
                    field_obj.value = field_obj.parse_value(request_handler.request.get_all(field_name))
                else:
                    field_obj.value = field_obj.parse_value(request_handler.request.get(field_name))

    def __setitem__(self, key, value):
        self.fields[key].value = value

    def __getitem__(self, key):
        return self.fields[key]

    def get(self, key, default=None):
        return self.fields.get(key, default)

    def clear_values(self):
        for field_name, field_obj in self.fields.iteritems():
            field_obj.value = None

    def validate(self):
        """
            Can be overwritten
        """
        return {}

    def is_valid(self):
        has_field_errors = False
        for field_name, field_obj in self.fields.iteritems():
            field_obj.error = None
            if field_obj.required and not field_obj.value:
                field_obj.error = _(u'Este campo es obligatorio')
                has_field_errors = True
            elif hasattr(field_obj, 'validator') and field_obj.validator:
                err_msg = field_obj.validator.validate(field_obj.value)
                if err_msg:
                    field_obj.error = err_msg
                    has_field_errors = True
                else:
                    field_obj.error = None

        err_dict = self.validate()
        for field_name, err_msg in err_dict.iteritems():
            self.fields[field_name].error = err_msg

        self.valid = not has_field_errors and not err_dict
        return self.valid
        
    def __unicode__(self):
        return ''.join([unicode(val) for key, val in self.fields.iteritems()])

    def __str__(self):
        return unicode(self)


class RecaptchaForm(Form):
    @property
    def recaptcha_format_html(self):
        options = {
            'theme': 'white',
            'lang': 'es',
        }
        html = '<script type="text/javascript">'
        html += 'var RecaptchaOptions = %s;' % json.dumps(options)
        html += '</script>'

        return html

    @property
    def recaptcha_html(self):
        html = '<label>%s</label>' % _(u'Introduzca los caracteres en la imagen')
        html += '''
            <script type="text/javascript"
                src="http://www.google.com/recaptcha/api/challenge?k=%s">
            </script>''' % settings.RECAPTCHA_PUBLIC_KEY
        html += '''
            <noscript>
            <iframe src="http://www.google.com/recaptcha/api/noscript?k=%s"
                 height="300" width="500" frameborder="0"></iframe><br/>
            <textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
            <input type="hidden" name="recaptcha_response_field" value="manual_challenge">
            </noscript>''' % settings.RECAPTCHA_PUBLIC_KEY

        if hasattr(self, 'recaptcha_ok') and not self.recaptcha_ok:
            html += '<p class="error-msg">%s</p>' % _(u'Los caracteres introducidos no son correctos')

        return html

    def is_valid(self):
        res = super(RecaptchaForm, self).is_valid()

        params = urllib.urlencode(dict(
            privatekey = settings.RECAPTCHA_PRIVATE_KEY,
            remoteip = self.request_handler.request.remote_addr,
            challenge = self.request_handler.request.get('recaptcha_challenge_field'),
            response = self.request_handler.request.get('recaptcha_response_field')))

        request = urllib2.Request(settings.RECAPTCHA_URL, params)
        response = urllib2.urlopen(request).read()

        self.recaptcha_ok = res and response[:len('true')] == 'true'
        self.valid = res and self.recaptcha_ok
        return self.valid

