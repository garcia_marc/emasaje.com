import jinja2
import settings
import i18n

def render(template_name, context={}):
    extensions = settings.JINJA2_EXTENSIONS
    if settings.I18N:
        extensions += ('jinja2.ext.i18n',)
    env = jinja2.Environment(loader=jinja2.FileSystemLoader(settings.JINJA2_TEMPLATES), extensions=extensions)
    if settings.I18N:
        env.install_gettext_callables(
            i18n.gettext,
            i18n.ngettext,
            newstyle=True,
        )
        env.globals.update({
            'format_date': i18n.format_date,
            'format_time': i18n.format_time,
            'format_datetime': i18n.format_datetime,
        })
    try:
        template = env.get_template(template_name)
    except jinja2.TemplateNotFound:
        raise jinja2.TemplateNotFound(template_name)
    return template.render(context)

def render_response(request, template_name, context={}):
    context.update(dict(settings=settings))
    request.response.out.write(render(template_name, context))

