import sys
import os
import webapp2

sys.path.append(os.path.join(os.path.dirname(__file__), 'ext'))
from lib.http import NotFoundHandler
import urls
import settings

urlmap = urls.urlmap + [('^.*$', NotFoundHandler),]
app = webapp2.WSGIApplication(urlmap, debug=settings.DEBUG)

