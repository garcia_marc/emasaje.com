# -*- coding: utf-8 -*-
import re
from lib.i18n import gettext as _

class RegexValidator:
    @classmethod
    def validate(cls, val):
        if not val or cls.regex.search(val):
            return None
        else:
            return cls.err_msg

class IsEmail(RegexValidator):
    err_msg = _(u'Dirección de correo incorrecta (debe tener el formato nombre@dominio.com)')
    regex = re.compile(
        r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"  # dot-atom
        r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-011\013\014\016-\177])*"' # quoted-string
        r')@(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?$', # domain
        re.IGNORECASE
    )

class IsUrl(RegexValidator):
    err_msg = _(u'Dirección de página web incorrecta (debe tener el formato http://www.dominio.com)')
    regex = re.compile(
        r'^https?://' # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?|' #domain...
        r'localhost|' #localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
        r'(?::\d+)?' # optional port
        r'(?:/?|[/?]\S+)$',
        re.IGNORECASE
    )

class IsSlug(RegexValidator):
    err_msg = _(u'No se pueden usar espacios, puntos (u otros signos de puntuación excepto el guión). Ni caráceteres acentuados, la ñ o la ç; y debe tener entre 6 y 50 carácteres. Valores correctos son <b>centro-masaje-logrono</b>, o <b>quiromasajista-ricardo-estalman</b>')
    regex = re.compile(r'^[a-z0-9\-]{6,50}$')

class IsPhone(RegexValidator):
    err_msg = _(u'Número de teléfono incorrecto')
    regex = re.compile(r'^[0-9\+\( \)\ \.]+$')

class IsPassword:
    @classmethod
    def validate(cls, val):
        if len(val) < 4:
            return _(u'La contraseña debe tener un mínimo de 4 carácteres')
        else:
            return None

