import datetime
import time
import cPickle
import hashlib
import random
import Cookie
from google.appengine.ext import db
from google.appengine.ext import webapp
import settings

COOKIE_NAME = 'ssidc'
RANDOM_BITS = 128

class Sessions(db.Model):
    ssid = db.StringProperty(indexed=True)
    created = db.DateTimeProperty(indexed=True)
    last_used = db.DateTimeProperty(indexed=True)
    content = db.BlobProperty(indexed=False)

class Session:
    def __init__(self, request_handler):
        self.request_handler = request_handler
        self.session_data = {}
        self.obj = None
        self.ssid = request_handler.request.cookies.get(COOKIE_NAME)
        if self.ssid:
            try:
                self.obj = Sessions.all().filter('ssid =', self.ssid).fetch(1)[0]
            except IndexError:
                pass
            else:
                self.session_data = cPickle.loads(self.obj.content)
        else:
            self.set_cookie()

    def __getitem__(self, key):
        return self.session_data[key]

    def get(self, key, default=None):
        return self.session_data.get(key, default)

    def __setitem__(self, key, value):
        self.session_data[key] = value

    def __delitem__(self, key):
        del self.session_data[key]

    def commit(self):
        if self.obj:
            self.obj.last_used = datetime.datetime.now()
            self.obj.content = cPickle.dumps(self.session_data)
            self.obj.put()
        else:
            Sessions(
                ssid = self.ssid,
                created = datetime.datetime.now(),
                last_used = datetime.datetime.now(),
                content = cPickle.dumps(self.session_data),
            ).put()

    def set_cookie(self):
        self.ssid = hashlib.sha256(settings.SECRET_KEY + ('%016d' % time.time()) + str(random.getrandbits(RANDOM_BITS))).hexdigest()

        cookie = Cookie.SimpleCookie()
        cookie[COOKIE_NAME] = self.ssid
        cookie[COOKIE_NAME]['path'] = '/'
        self.request_handler.response.headers['Set-Cookie'] = cookie.output(None, '')

