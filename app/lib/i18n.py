
def gettext(value):
    return value

def ngettext(singular, plural, number):
    if number == 1:
        return singular
    else:
        return plural

def format_date(value):
    return str(value)

def format_time(value):
    return str(value)

def format_datetime(value):
    return str(value)

