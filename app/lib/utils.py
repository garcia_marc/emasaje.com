
def paginate(qs, order_by, paginate_by, next_result, field_type=str):
    order_field = order_by
    cmp_op = '>= '
    if order_by[0] == '-':
        order_field = order_by[1:]
        cmp_op = '<= '
    if next_result:
        qs.filter(order_field + ' ' + cmp_op, field_type(next_result))
        next_result = None
    qs = qs.order(order_by)
    res = qs.fetch(paginate_by + 1)
    if len(res) > paginate_by:
        next_result = getattr(res[-1], order_field)
        res = res[:-1]
    return res, next_result

