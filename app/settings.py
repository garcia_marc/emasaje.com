# -*- coding: utf-8 -*-
import os
from lib.i18n import gettext as _

I18N = True
MAIL_FROM = 'www.emasaje.com <info@emasaje.com>'
MAIL_TO = 'info@emasaje.com'
SECRET_KEY = 'wExtr2(m6p%dMctULvADWBogtbB7Ogo43hx1J)V2pW)xVLetMd?rYF0dEqV72ox0iGPZS2ZHu9kCL4MZnF%uUZ7HdlEUgDOEXv.Xz0G%q2ttRYcy0GdP.e?%dC0%ywVD'
TEMPLATE_APPS = (
    'home',
    'search',
    'pages',
    'navigation',
    'accounts',
    'dashboard',
    'stats',
    'legal',
    'admin',
)
JINJA2_TEMPLATES = [os.path.join(os.path.dirname(__file__), 'templates')]
JINJA2_TEMPLATES += [os.path.join(os.path.dirname(__file__), 'apps', app, 'templates') for app in TEMPLATE_APPS]
JINJA2_EXTENSIONS = ('jinja2.ext.i18n',)
RESERVED_SLUGS = (
    u'accounts',
    u'dashboard',
    u'files',
    u'control',
    u'tasks',
    u'legal',
    u'masajistas',

    u'digitopuntura',
    u'drenaje-linfatico',
    u'fisioterapia',
    u'masaje-anticelulitico',
    u'masaje-ayurvedico',
    u'masaje-californiano',
    u'masaje-circulatorio',
    u'masaje-con-piedras',
    u'masaje-deportivo',
    u'masaje-descontracturante',
    u'masaje-hawaiano',
    u'masaje-prenatal',
    u'masaje-relajante',
    u'masaje-sueco',
    u'masaje-tailandes',
    u'masaje-terapeutico',
    u'osteopatia',
    u'quiromasaje',
    u'quiropractica',
    u'reflexologia-podal',
    u'reiki',
    u'shiatsu',
)
ANALYTICS_SOURCE = 'www.emasaje.com'
ANALYTICS_USER = 'analytics@emasaje.com'
ANALYTICS_PASSWORD = 'happy.ending.69'
ANALYTICS_PROFILE = '21198445'
ANALYTICS_SERVICE = 'analytics'
ANALYTICS_ACCOUNT_TYPE = 'GOOGLE'
STATS_PERIOD_DAYS = 30

JSAPI_KEY = 'ABQIAAAACJUP9A3Ol8ETsTjNHIwwuxRr7l-fdrKr9VE5Uz0sGakHbcyNExSGK-cd11WYRdpLvQ39jl2NO9M7qw'

RECAPTCHA_URL = 'http://www.google.com/recaptcha/api/verify'
RECAPTCHA_PUBLIC_KEY = '6LclfPcSAAAAANZ1x-z4_oclqIOSAM5jKkjmIj_J'
RECAPTCHA_PRIVATE_KEY = '6LclfPcSAAAAAFA5nmRiqvNJnI2k2rQT-7uBCEfP'

CONDITIONS_VERSION = 0
NAV_PAGINATE_BY = 30

MAP_URBAN_SECTOR_LNG_SIZE = 3
MAP_URBAN_SECTOR_LNG_DIFF = 1
MAP_URBAN_SECTOR_LAT_SIZE = 2
MAP_URBAN_SECTOR_LAT_DIFF = 1
MAP_URBAN_ZOOM = 13
MAP_URBAN_MIN_RESULTS = 5

MAP_RURAL_SECTOR_LNG_SIZE = 40
MAP_RURAL_SECTOR_LNG_DIFF = 10
MAP_RURAL_SECTOR_LAT_SIZE = 30
MAP_RURAL_SECTOR_LAT_DIFF = 10
MAP_RURAL_ZOOM = 11

MAP_SHOW_RESULTS = 5


EMAIL_PAGES = {
    'gmail.com': 'http://www.gmail.com',
    'hotmail.com': 'http://www.hotmail.com',
    'yahoo.es': 'http://www.yahoo.es',
}

devel_str = 'Google App Engine Development'
server_software = os.environ.get('SERVER_SOFTWARE')
if server_software and server_software[:len(devel_str)] != devel_str:
    DEBUG = False
else:
    DEBUG = True

GENDER = (
   (0, _(u'Hombre')),
   (1, _(u'Mujer')),
)
LANGUAGES = (
    (0, _(u'Español')),
    (1, _(u'Catalán')),
    (2, _(u'Basco')),
    (3, _(u'Gallego')),
    (4, _(u'Inglés')),
    (5, _(u'Francés')),
    (6, _(u'Portugués')),
    (7, _(u'Italiano')),
    (8, _(u'Alemán')),
)
PAYMENT_METHODS = (
    (0, _(u'Efectivo')),
    (1, _(u'Visa')),
    (2, _(u'Master Card')),
    (3, _(u'American Express')),
)
PAGE_IMAGES = {
    0: (0, 'image0.jpeg'),
    1: (1, 'image1.jpeg'),
    2: (2, 'image2.jpeg'),
    3: (3, 'image3.jpeg'),
}
PAGE_THEMES = {
    0: (0, '#465f40'),
    1: (1, '#2b7ece'),
    2: (2, '#2d1120'),
    3: (3, '#d69a54'),
    4: (4, '#268d84'),
    5: (5, '#ad860e'),
}
MAIN_CITIES = [
    ('Madrid', 3117735, 40.416633, -3.700333, 13),
    ('Barcelona', 3128760, 41.387885, 2.169971, 13),
    ('Valencia', 000000, 39.470125, -0.376968, 14),
    ('Sevilla', 2510911, 37.382707, -5.996304, 14),
    ('Bilbao', 3128026, 43.256956, -2.923479, 15),
    ('Malaga', 00000, 36.719623, -4.419937, 14),
]

