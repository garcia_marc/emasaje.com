from apps.home.handlers import Home
from apps.search.handlers import Search
from apps.pages.handlers import PageHandler
from apps.accounts import handlers as accounts
from apps.dashboard import handlers as dashboard
from apps.dashboard import services as dashboard_services
from apps.stats import handlers as stats
from apps.legal import handlers as legal
from apps.admin import handlers as control
from apps.navigation.handlers import Methods, Region
from apps.tasks import handlers as tasks

urlmap = [
    # home
    (r'^/$', Home),

    # search
    (r'^/search/?$', Search),

    # accounts
    (r'^/accounts/info/?$', accounts.Info),
    (r'^/accounts/sample/?$', accounts.Sample),
    (r'^/accounts/register/?$', accounts.RegisterAccount),
    (r'^/accounts/login/?$', accounts.LoginAccount),
    (r'^/accounts/logout/?$', accounts.LogoutAccount),
    (r'^/accounts/pending-data/?$', accounts.PendingData),
    (r'^/accounts/activate/(?P<email_user>[\w\.-]+)/(?P<email_domain>[\w\.-]+)/(?P<activation_key>[\w]+)/?$', accounts.ActivateAccount),
    (r'^/accounts/password-recovery/?$', accounts.PasswordRecoveryStep1),
    (r'^/accounts/password-recovery/(?P<email_user>[\w\.-]+)/(?P<email_domain>[\w\.-]+)/(?P<activation_key>[\w]+)/?$', accounts.PasswordRecoveryStep2),

    # dashboard
    (r'^/dashboard/?$', dashboard.Home),
    (r'^/dashboard/address/?$', dashboard.Address),
    (r'^/dashboard/contact-info/?$', dashboard.ContactInfo),
    (r'^/dashboard/page-content/?$', dashboard.PageContent),
    (r'^/dashboard/massage-methods/?$', dashboard.MassageMethods),
    (r'^/dashboard/schedule/?$', dashboard.Schedule),
    (r'^/dashboard/theme-gallery/?$', dashboard.ThemeGallery),
    (r'^/dashboard/image-gallery/?$', dashboard.ImageGallery),

    # stats
    (r'^/dashboard/stats/by-source/?$', stats.StatsBySource),
    (r'^/dashboard/stats/by-region/?$', stats.StatsByRegion),
    (r'^/dashboard/stats/by-date/?$', stats.StatsByDate),

    (r'^/dashboard/services/get-states/?$', dashboard_services.States),
    (r'^/dashboard/services/get-provinces/?$', dashboard_services.Provinces),
    (r'^/dashboard/services/get-cities/?$', dashboard_services.Cities),
    (r'^/dashboard/services/help-request/?$', dashboard_services.HelpRequest),

    # legal
    (r'^/legal/privacy-policy/?$', legal.PrivacyPolicy),
    (r'^/legal/terms-and-conditions/?$', legal.TermsConditions),

    # navigation
    (r'^/masajistas/por-especialidad/?$', Methods),
    (r'^/masajistas/por-especialidad/(?P<slug>[a-z0-9\-]+)/?$', Methods),
    (r'^/masajistas/por-region/?$', Region),
    (r'^/masajistas/por-region/(?P<region_slug>[a-z0-9\-/]+[a-z0-9\-])/?$', Region),

    # appengine admin
    (r'^/admin/review/?$', control.Review),
    (r'^/admin/impersonate/?$', control.Impersonate),
    (r'^/admin/newsletter/?$', control.Newsletter),

    # tasks
    (r'^/tasks/session-clean-up/?$', tasks.SessionCleanUp),
    (r'^/tasks/geocode-one/?$', tasks.GeocodeOne),
    (r'^/tasks/geocode-all/?$', tasks.GeocodeAll),
    (r'^/tasks/touch-all/?$', tasks.TouchAll),
    (r'^/tasks/newsletter-one/?$', tasks.NewsletterOne),
    (r'^/tasks/newsletter-all/?$', tasks.NewsletterAll),

    # page
    (r'^/(?P<slug>[a-z0-9\-]+)/?$', PageHandler),
]

